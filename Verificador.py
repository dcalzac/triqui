class verificador:
    
    def _init_(self):
        self.triqui=False
        self.matriz=""
        
    def verificarTriqui(self):
        self.triqui=self.verificarfilas()
            
        if not self.triqui:
            self.triqui=self.verificarColumnas()
        
        if not self.triqui:
            self.triqui=self.verificarDiagonales()
            
        return self.triqui
    
    def verificarFilas (self, tabl):
        
        for fila in range (0,3):
            if tabl [fila][0] == tabl [fila] [1] and tabl [fila] [0] == tabl [fila] [2]:
                self.triqui=True
        return self.triqui 
    
    def verificarColumnas (self, tabl):
        for columna in range (0,3):
            if tabl [columna][0] == tabl [columna] [1] and tabl [columna] [0] == tabl [columna] [2]:
                self.triqui=True
        return self.triqui
    
    def verificarDiagonales (self, tabl):
        if tabl [0][0] == tabl [1] [1] and tabl [0] [0] == tabl [2] [2]:
                self.triqui=True
        if tabl [0][2] == tabl [1] [1] and tabl [0] [2] == tabl [2] [0]:
                self.triqui=True